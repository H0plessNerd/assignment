﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;


namespace ASEAssignment
{
    class Canvas
    {
        Graphics g;
        Pen Pen;
        int xPos = 0, yPos = 0; // x and y positions for drawing
        Boolean Fill = false;
        public string CurrentColour;
        
        /// <summary>
        /// creates the canvas to be drawn on 
        /// </summary>
        /// <param name="g">a reference to the inherited Graphics class</param>
        public Canvas(Graphics g)
        {
            this.g = g;
            //Pen = new Pen(Color.Black, 1); //standard pen (black)
            Pen = new Pen(Color.Black, 1); //standard pen (black)

            CurrentColour = "black";
        }

        /// <summary>
        /// draws a straight line from the current x and y positions to toX and toY
        /// </summary>
        /// <param name="toX">the new x position</param>
        /// <param name="toY">the new y position</param>
        public void DrawLine(int toX, int toY)
        {
            g.DrawLine(Pen, xPos, yPos, toX, toY);
            xPos = toX;
            yPos = toY;

        }

        /// <summary>
        /// draws a square from the current x and y positions with each side being 'width' long
        /// </summary>
        /// <param name="width">the length of the sides of the square </param>
        public void DrawSquare(int width)
        {
            if (CurrentColour.Equals("redgreen"))
            {
                
                //Threading.redgreenrect();
                if (Fill == false)
                {
                    g.DrawRectangle(Pen, xPos, yPos, xPos + width, yPos + width);
                }
                else
                {
                    using (Brush brush = new SolidBrush(Pen.Color))
                    {
                        g.FillRectangle(brush, xPos, yPos, width, width);
                    }
                }
            }
            else
            {

                if (Fill == false)
                {
                    //Console.WriteLine("pen colour: " + Pen.Color);
                    //Console.WriteLine("pen colour: " + CurrentColour);
                    g.DrawRectangle(Pen, xPos, yPos, xPos + width, yPos + width);
                }
                else
                {
                    using (Brush brush = new SolidBrush(Pen.Color))
                    {
                        g.FillRectangle(brush, xPos, yPos, width, width);
                    }
                }
            }
        }

        /// <summary>
        /// draws a rectangle from the current x and y positions with 2 sides oposite being 'width' long and the other 'height' long
        /// </summary>
        /// <param name="width">the width of the rectange</param>
        /// <param name="height">the heigth of the rectangle</param>
        public void DrawRect(int width, int height)
        {
            if (Fill == false)
            {
                g.DrawRectangle(Pen, xPos, yPos, xPos + width, yPos + height);
            }
            else
            {
                using (Brush brush = new SolidBrush(Pen.Color))
                {
                    g.FillRectangle(brush, xPos, yPos, width, height);
                }
            }
        }

        /// <summary>
        /// draws a circle from the current x and y positions with a radius of 'radius'
        /// </summary>
        /// <param name="radius">the radius of the circle </param>
        public void DrawCircle(int radius)
        {
            //Console.WriteLine("current: " + CurrentColour); 
            if (Fill == false)
            {
                g.DrawEllipse(Pen, xPos - radius, yPos - radius, radius + radius, radius + radius);
            }
            else
            {
                using (Brush brush = new SolidBrush(Pen.Color))
                {
                    g.FillEllipse(brush, xPos, yPos, radius, radius);
                }
            }
        }

        /// <summary>
        /// draws a right angled triangle from the current x and y positions with each side being 'length' long
        /// </summary>
        /// <param name="length"></param>
        public void DrawTri(int length)
        {
            int toX = xPos + length;
            int toY = yPos + length;
            Point point1 = new Point(xPos, yPos);
            Point point2 = new Point(toX, yPos);
            Point point3 = new Point(xPos, toY);
            Point[] curvePoints = {
                    point1,
                    point2,
                    point3
                };
            if (Fill == false)
            {
                g.DrawPolygon(Pen, curvePoints);

            }
            else
            {
                using (Brush brush = new SolidBrush(Pen.Color))
                {
                    g.FillPolygon(brush, curvePoints);
                }
            }
            
        }

        /// <summary>
        /// changes the current x and y positions to toX and toY without drawing onto the canvas
        /// </summary>
        /// <param name="toX">the new x position</param>
        /// <param name="toY">the new y position</param>
        public void MovePen(int toX, int toY)
        {
            xPos = toX;
            yPos = toY;
        }

        /// <summary>
        /// changes the pen colour to col
        /// </summary>
        /// <param name="col">the new pen colour</param>
        public void PenColour(string col)
        {
            if (col == "red")
            {
                Pen.Color = Color.Red;
                CurrentColour = "red";
            }
            else if (col == "green")
            {
                Pen.Color = Color.Green;
                CurrentColour = "green";
            }
            else if (col == "blue")
            {
                Pen.Color = Color.Blue;
                CurrentColour = "blue";
            }
            else if (col == "yellow")
            {
                Pen.Color = Color.Yellow;
                CurrentColour = "yellow";
            }
            else if (col == "black")
            {
                Pen.Color = Color.Black;
                CurrentColour = "black";
            }
            else if (col == "redgreen")
            {
                CurrentColour = "redgreen";
            }
            else if (col == "redblue")
            {
                CurrentColour = "redblue";
            }
            else if (col == "bluegreen")
            {
                CurrentColour = "bluegreen";
            }
            else if (col == "yellowblack")
            {
                CurrentColour = "yellowblack";
            }

            else
            {
                Console.WriteLine("colour not an option: " + col);
            }
            
        }

        /// <summary>
        /// clears the canvas back to being a blank white but saves the current x and y positions, the current pen colour and whether fill is set to on or off
        /// </summary>
        public void Clear()
        {
            g.Clear(Color.White);
        }

        /// <summary>
        /// clears the canvas to a blank white, sets the x and y positions back to 0,0 and the pen back to black (the same settings as when started)
        /// </summary>
        public void Reset()
        {
            g.Clear(Color.White);
            Pen.Color = Color.Black;
            xPos = 0;
            yPos = 0; 
        }
        
        /// <summary>
        /// dictates whether the following shape commands should be drawn as an outline or a solid shape
        /// </summary>
        /// <param name="set">either on or off for an outline or solid shapre resepectivley</param>
        public void fill(string set)
        {
            if (set == "on")
            {
                Fill = true;
            }
            else if (set == "off")
            {
                Fill = false;
            }
            else
                Console.WriteLine("error");
        }

    }



}
