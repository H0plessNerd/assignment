﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace ASEAssignment
{
    public partial class Form1 : Form
    {
        Bitmap OutputBitmap = new Bitmap(640,480);
        Canvas MyCanvas;
        //Thread newThread;
        //Boolean flag = false, running = false;
        bool run = true;
        bool loop = false;
        int iterations, loopline, index, lines, lineread;
        int i = 0;
        int errors = 0;

        public Form1()
        {
            InitializeComponent();
            //newThread = new Thread(redgreenrect);

            //newThread.Start();
            MyCanvas = new Canvas(Graphics.FromImage(OutputBitmap));
            MyCanvas.Clear();
            Program.Clear();
            
            label1.Text = " ";
            label2.Text = " ";
            
        }
        

        /// <summary>
        /// clears the program text box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {            
            Program.Clear();          
        }

        public Dictionary<string, int> VariableDict = new Dictionary<string, int>();

        public Dictionary<string, int> SyntaxDict = new Dictionary<string, int>();
        
        /// <summary>
        /// checks the character input into the command line text box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Command_line_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                string Command = Command_line.Text.Trim().ToLower();
                string[] commands = Command.Split(' ');
                
                ReadCommands(commands);
                Command_line.Text = ""; //clear the command line
                Refresh();
                
            }
        }

    
        /// <summary>
        /// reads the contents of the program text box
        /// </summary>
        private void ProgramReader()
        {
            String Code = Program.Text.Trim().ToLower();
            Code = Code.Trim('\r');
            string[] commands = Code.Split('\n');
            lines = Program.Lines.Count();

            try
            {

                for (int currentline = 0; currentline < lines; currentline++)
                {
                    lineread = currentline;
                    string[] command = commands[currentline].Split(' ');

                    if (run)
                    {
                        ReadCommands(command);
                    }
                    else
                    {
                        currentline = currentline + 1;
                        run = true;
                        Console.WriteLine("not run");
                    }
                }
            }
            // If the program text box has a tailing blank line, then the number of lines to parse is 1 less
            catch
            {
                for (int currentline = 0; currentline < lines; currentline++)
                {
                    lineread = currentline;
                    lines = lines - 1;
                    string[] command = commands[currentline].Split(' ');
                    if (run)
                    {
                        ReadCommands(command);
                    }
                    else
                    {
                        currentline = currentline + 1;
                    }
                }
            }
            Refresh();
        }

        /// <summary>
        /// creates the drawing window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Drawing_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.DrawImageUnscaled(OutputBitmap, 0, 0);
        }

        /// <summary>
        /// runs a syntax check on the contents of the program text box without running any of the commands
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            //check syntax of program window before running
            run = false;
            String Code = Program.Text.Trim().ToLower();
            Code = Code.Trim('\r');
            string[] commands = Code.Split('\n');
            lines = Program.Lines.Count();
            lines = lines + 1;
            syntax1.Clear();

            try
            {
                for (int currentline = 0; currentline < lines; currentline++)
                {

                    string[] command = commands[currentline].Split(' ');
                    bool variable = VariableDict.ContainsKey(command[0]);
                    if (command[0].Equals("moveto") || command[0].Equals("drawto") || command[0].Equals("square") || command[0].Equals("rect") || command[0].Equals("circle") || command[0].Equals("triangle") || command[0].Equals("colour") || command[0].Equals("fill") || command[0].Equals("var") || command[0].Equals("if") || command[0].Equals("endif") || variable)
                    {
                        //correct commands
                    }
                    else
                    {
                        Console.WriteLine("command: " + command[currentline]);
                        string SyntaxStr = command[0];
                        errors = errors + 1;
                        Console.WriteLine("error: " + SyntaxStr);
                        Console.WriteLine("line: " + currentline);
                        Console.WriteLine("errors no: " + errors);
                        VariableDict[SyntaxStr] = errors;
                        syntax1.AppendText("Invalid command " + command[0] + " on line: " + (currentline + 1) + '\n');
                    }
                    int num;
                    bool number = int.TryParse(command[1], out num);
                    bool var = VariableDict.ContainsKey(command[1]);
                    //red green blue yellow black = on off
                    try
                    {

                        if (number || var || command[1].Equals("red") || command[1].Equals("green") || command[1].Equals("blue") || command[1].Equals("black") || command[1].Equals("red") || command[1].Equals("=") || command[1].Equals("redgreen") || command[1].Equals("on") || command[1].Equals("off"))
                        {
                            //correct params - number or variable

                        }
                        else
                        {
                            syntax1.AppendText("Invalid parameter: " + command[1] + " on line: " + (currentline + 1) + '\n');
                        }
                    }
                    catch
                    {
                        Console.WriteLine("got here");
                    }
                }
            }
            // If the program text box has a tailing blank line, then the number of lines to parse is 1 less
            catch
            {
                for (int currentline = 0; currentline != lines; currentline++)
                {
                    lines = lines - 1;
                    string[] command = commands[currentline].Split(' ');
                    bool variable = VariableDict.ContainsKey(command[0]);

                    try
                    {
                        if (command[0].Equals("moveto") || command[0].Equals("drawto") || command[0].Equals("square") || command[0].Equals("rect") || command[0].Equals("circle") || command[0].Equals("triangle") || command[0].Equals("colour") || command[0].Equals("fill") || command[0].Equals("var") || command[0].Equals("if") || command[0].Equals("endif") || variable)
                        {
                            //correct commands output
                        }
                        else
                        {
                            string SyntaxStr = command[0];
                            errors = errors + 1;
                            Console.WriteLine("error: " + SyntaxStr);
                            Console.WriteLine("line: " + currentline);
                            Console.WriteLine("errors no: " + errors);

                            VariableDict[SyntaxStr] = errors;
                            syntax1.AppendText("Invalid command " + command[0] + " on line: " + (currentline + 1) + '\n');
                        }
                        int num;
                        bool number = int.TryParse(command[1], out num);
                        bool var = VariableDict.ContainsKey(command[1]);
                        try
                        {
                            if (number || var || command[1].Equals("red") || command[1].Equals("green") || command[1].Equals("blue") || command[1].Equals("black") || command[1].Equals("yellow") || command[1].Equals("redgreen") || command[1].Equals("on") || command[1].Equals("off") || command[1].Equals("="))
                            {
                                //correct params - number variable on off =

                            }
                            else
                            {
                                syntax1.AppendText("Invalid parameter: " + command[1] + " on line: " + (currentline + 1) + '\n');
                            }
                        }
                        catch
                        {
                            //no parameters
                            Console.WriteLine("catch got here");
                        }
                    }
                    catch
                    {
                        //no params
                        Console.WriteLine("exception here");

                    }

                }
            }
            int er = syntax1.Lines.Count();
            //if (er < 1)
            //{
            //    syntax1.AppendText("No syntax errors");
            //}
            
        }
        /// <summary>
        /// runs the command depending on the contents of the param commands
        /// </summary>
        /// <param name="commands">the line of commands from either the command line text box or a single line from the program text box</param>

            public void ReadCommands(string[] commands)
        {
            label1.Text = " ";
            label2.Text = " ";
            bool variable = VariableDict.ContainsKey(commands[0]);
            if (commands[0].Equals("drawto") == true)
            {
                foreach (var value in commands)
                {
                    int xPos, yPos;
                    bool success = int.TryParse(commands[1], out xPos);
                    if (success)
                    {
                        success = int.TryParse(commands[2], out yPos);
                        if (success)
                        {
                            MyCanvas.DrawLine(xPos, yPos);
                        }
                        else
                        {
                            label2.Text = "Invalid y position: " + commands[2];

                        }
                    }
                    else
                    {
                        label2.Text = "Invalid x position: " + commands[1];
                    }
                }
            }
            else if (commands[0].Equals("square") == true)
            {
                foreach (var value in commands)
                {
                    if (MyCanvas.CurrentColour.Equals("red") || MyCanvas.CurrentColour.Equals("green") || MyCanvas.CurrentColour.Equals("blue") || MyCanvas.CurrentColour.Equals("yellow") || MyCanvas.CurrentColour.Equals("black"))
                    {
                        int len;
                        bool success = int.TryParse(commands[1], out len);
                        if (success)
                        {
                            MyCanvas.DrawSquare(len);
                        }
                        else if (VariableDict.ContainsKey(commands[1]) == true)
                        {
                            int length = VariableDict[commands[1]];

                            MyCanvas.DrawSquare(length);
                        }
                        else
                        {
                            label2.Text = "Invalid parameter: " + commands[1];
                        }
                    }
                    else if (MyCanvas.CurrentColour.Equals("redgreen") || MyCanvas.CurrentColour.Equals("redblue") || MyCanvas.CurrentColour.Equals("bluegreen") || MyCanvas.CurrentColour.Equals("yellowblack"))
                    {

                    }
                }
            }
            else if (commands[0].Equals("rectangle") == true)
            {
                foreach (var value in commands)
                {
                    int width, height;
                    bool success = int.TryParse(commands[1], out width);
                    if (success)
                    {
                        success = int.TryParse(commands[2], out height);
                        if (success)
                        {
                            MyCanvas.DrawRect(width, height);
                        }
                        else if (VariableDict.ContainsKey(commands[2]) == true)
                        {
                            int heightV = VariableDict[commands[2]];

                            MyCanvas.DrawRect(width, heightV);
                        }
                        else
                        {
                            label2.Text = "Invalid height: " + commands[2];
                        }
                    }
                    else if (VariableDict.ContainsKey(commands[1]) == true)
                    {
                        int widthV = VariableDict[commands[1]];

                        MyCanvas.DrawSquare(widthV);
                    }
                    else
                    {
                        label2.Text = "Invalid width: " + commands[1];
                    }
                }
            }
            else if (commands[0].Equals("circle") == true)
            {
                foreach (var value in commands)
                {
                    int radius;
                    bool success = int.TryParse(commands[1], out radius);
                    if (success)
                    {
                        MyCanvas.DrawCircle(radius);
                    }
                    else if (VariableDict.ContainsKey(commands[1]) == true)
                    {
                        int radiusV = VariableDict[commands[1]];

                        MyCanvas.DrawCircle(radiusV);
                    }
                    else
                    {
                        label2.Text = "Invalid radius: " + commands[1];
                    }
                }

            }
            else if (commands[0].Equals("triangle") == true)
            {
                foreach (var value in commands)
                {
                    int len;
                    bool success = int.TryParse(commands[1], out len);
                    if (success)
                    {
                        MyCanvas.DrawTri(len);
                    }
                    else if (VariableDict.ContainsKey(commands[1]) == true)
                    {
                        int length = VariableDict[commands[1]];

                        MyCanvas.DrawTri(length);
                    }
                    else
                    {
                        label2.Text = "Invalid length: " + commands[1];
                    }
                }
            }
            else if (commands[0].Equals("moveto") == true)
            {
                foreach (var value in commands)
                {
                    int xPos, yPos;
                    bool success = int.TryParse(commands[1], out xPos);
                    if (success)
                    {
                        success = int.TryParse(commands[2], out yPos);
                        if (success)
                        {
                            MyCanvas.MovePen(xPos, yPos);

                        }
                        else if (VariableDict.ContainsKey(commands[2]) == true)
                        {
                            int yPosV = VariableDict[commands[2]];

                            MyCanvas.MovePen(xPos, yPosV);
                        }
                        else
                        {
                            label2.Text = "Invalid y postion: " + commands[2];
                        }
                    }
                    else if (VariableDict.ContainsKey(commands[1]) == true)
                    {
                        int xPosV = VariableDict[commands[1]];
                        int.TryParse(commands[2], out yPos);

                        MyCanvas.MovePen(xPosV, yPos);
                    }
                    else
                    {
                        label2.Text = "Invalid x position: " + commands[1];
                    }
                }
            }
            else if (commands[0].Equals("colour"))
            {
                string col = commands[1];
                Console.WriteLine(col);
                MyCanvas.PenColour(col);
                if (col.Equals("black") || col.Equals("red") || col.Equals("blue") || col.Equals("yellow") || col.Equals("green"))
                {
                    MyCanvas.PenColour(col);

                    //Console.WriteLine("colour: " + col);

                }
                else if (col.Equals("redgreen"))
                {
                    //newThread.Start();
                }
                else
                {
                    label2.Text = "Colour not available: " + commands[1];
                }
            }
            else if (commands[0].Equals("clear") == true)
            {
                MyCanvas.Clear();
            }
            else if (commands[0].Equals("reset") == true)
            {
                MyCanvas.Reset();
            }
            else if (commands[0].Equals("run") == true)
            {
                //run the code from the Program text box
                ProgramReader();
            }
            else if (commands[0].Equals("fill"))
            {
                string set = commands[1];
                if (set.Equals("on") || set.Equals("off"))
                {
                    MyCanvas.fill(set);
                }
                else
                {
                    label2.Text = "Invalid fill parameter: " + commands[1];
                }

            }
            else if (commands[0].Equals("var"))
            {
                string VarName = commands[1];
                string VarStr = commands[3];
                int VarInt = Int32.Parse(VarStr);
                VarName = VarName.Trim(' ');
                VarName = VarName.Trim('\n');
                VarName = VarName.Trim('\r');
                VariableDict[VarName] = VarInt;
                Console.WriteLine("Var: " + VarName + " : " + VarInt);


            }
            else if (commands[0].Equals("if"))
            {
                run = true;
                //commands[0] = if
                //commands[1] = first element
                //commands[2] = operator 
                //commands[3] = second element
                //commands[4] = 'then' or ... commands[0] = endif
                if (commands[2].Equals(">") || commands[2].Equals("<") || commands[2].Equals("=") || commands[2].Equals("<=") || commands[2].Equals(">=") || commands[2].Equals("!="))
                {

                    string check = commands[2];
                    int var, num;
                    bool success = int.TryParse(commands[1], out var);

                    if (check.Equals("="))
                    {
                        //if commands[1] is a number
                        if (success)
                        {
                            bool success2 = int.TryParse(commands[3], out num);
                            //if commands[3] is a number
                            if (success2)
                            {
                                if (var == num)
                                {
                                    Console.WriteLine("Valid");
                                    run = true;
                                }
                                else
                                {
                                    Console.WriteLine("invalid");
                                    run = false;
                                }
                            }
                            //if commands[3] is a pre set variable
                            else if (VariableDict.ContainsKey(commands[3]) == true)
                            {
                                int number = VariableDict[commands[3]];

                                if (var == number)
                                {
                                    Console.WriteLine("Valid");
                                    run = true;
                                }
                                else
                                {
                                    Console.WriteLine("invalid");
                                    run = false;
                                }

                            }
                            //if commands[3] is not a number or a pre set variable
                            else
                            {
                                Console.WriteLine("not valid");
                                run = false;
                            }
                        }
                        //if commands[1] is a pre set variable 
                        else if (VariableDict.ContainsKey(commands[1]) == true)
                        {
                            bool success2 = int.TryParse(commands[3], out num);
                            var = VariableDict[commands[1]];
                            //if commands[3] is number 
                            if (success2)
                            {
                                if (var == num)
                                {
                                    Console.WriteLine("Valid");
                                    run = true;

                                }
                                else
                                {
                                    Console.WriteLine("invalid");
                                    run = false;
                                }
                            }
                            //if commands[3] is a pre set variable
                            else if (VariableDict.ContainsKey(commands[3]) == true)
                            {
                                int number = VariableDict[commands[3]];

                                if (var == number)
                                {
                                    Console.WriteLine("Valid");
                                    run = true;
                                }
                                else
                                {
                                    Console.WriteLine("invalid");
                                    run = false;
                                }
                            }
                            //if commands[3] is not a number or a pre set variable 
                            else
                            {
                                Console.WriteLine("invalid");
                                run = false;
                            }
                        }
                        else
                        {
                            Console.WriteLine("invalid 2");
                            run = false;
                        }
                    }
                    else if (check.Equals(">"))
                    {
                        //if commands[1] is a number
                        if (success)
                        {
                            bool success2 = int.TryParse(commands[3], out num);
                            //if commands[3] is a number
                            if (success2)
                            {
                                if (var > num)
                                {
                                    run = true;
                                }
                            }
                            //if commands[3] is a pre set variable
                            else if (VariableDict.ContainsKey(commands[3]) == true)
                            {
                                int number = VariableDict[commands[3]];

                                if (var > number)
                                {
                                    Console.WriteLine("Valid");
                                    run = true;
                                }

                            }
                            //if commands[3] is not a number or a pre set variable
                            else
                            {
                                Console.WriteLine("not valid");
                            }
                        }
                        //if commands[1] is a pre set variable 
                        else if (VariableDict.ContainsKey(commands[1]) == true)
                        {
                            bool success2 = int.TryParse(commands[3], out num);
                            var = VariableDict[commands[1]];
                            //if commands[3] is number 
                            if (success2)
                            {
                                if (var > num)
                                {
                                    Console.WriteLine("Valid");
                                    run = true;
                                }
                            }
                            //if commands[3] is a pre set variable
                            else if (VariableDict.ContainsKey(commands[3]) == true)
                            {
                                int number = VariableDict[commands[3]];

                                if (var > number)
                                {
                                    Console.WriteLine("Valid");
                                    run = true;
                                }
                            }
                            //if commands[3] is not a number or a pre set variable 
                            else
                            {
                                Console.WriteLine("invalid");
                                run = false;
                            }
                        }
                        else
                        {
                            Console.WriteLine("invalid 2");
                            run = false;
                        }
                    }
                    else if (check.Equals("<"))
                    {
                        //if commands[1] is a number
                        if (success)
                        {
                            bool success2 = int.TryParse(commands[3], out num);
                            //if commands[3] is a number
                            if (success2)
                            {
                                if (var < num)
                                {
                                    Console.WriteLine("Valid");
                                    run = true;
                                }
                            }
                            //if commands[3] is a pre set variable
                            else if (VariableDict.ContainsKey(commands[3]) == true)
                            {
                                int number = VariableDict[commands[3]];

                                if (var < number)
                                {
                                    Console.WriteLine("Valid");
                                    run = true;
                                }

                            }
                            //if commands[3] is not a number or a pre set variable
                            else
                            {
                                Console.WriteLine("not valid");
                            }
                        }
                        //if commands[1] is a pre set variable 
                        else if (VariableDict.ContainsKey(commands[1]) == true)
                        {
                            bool success2 = int.TryParse(commands[3], out num);
                            var = VariableDict[commands[1]];
                            //if commands[3] is number 
                            if (success2)
                            {
                                if (var < num)
                                {
                                    Console.WriteLine("Valid");
                                    run = true;
                                }
                            }
                            //if commands[3] is a pre set variable
                            else if (VariableDict.ContainsKey(commands[3]) == true)
                            {
                                int number = VariableDict[commands[3]];

                                if (var < number)
                                {
                                    Console.WriteLine("Valid");
                                    run = true;
                                }
                            }
                            //if commands[3] is not a number or a pre set variable 
                            else
                            {
                                Console.WriteLine("invalid");
                                run = false;
                            }
                        }
                        else
                        {
                            Console.WriteLine("invalid 2");
                            run = false;
                        }
                    }
                    else if (check.Equals(">="))
                    {
                        //if commands[1] is a number
                        if (success)
                        {
                            bool success2 = int.TryParse(commands[3], out num);
                            //if commands[3] is a number
                            if (success2)
                            {
                                if (var >= num)
                                {
                                    Console.WriteLine("Valid");
                                    run = true;
                                }
                            }
                            //if commands[3] is a pre set variable
                            else if (VariableDict.ContainsKey(commands[3]) == true)
                            {
                                int number = VariableDict[commands[3]];

                                if (var >= number)
                                {
                                    Console.WriteLine("Valid");
                                    run = true;
                                }

                            }
                            //if commands[3] is not a number or a pre set variable
                            else
                            {
                                Console.WriteLine("not valid");
                            }
                        }
                        //if commands[1] is a pre set variable 
                        else if (VariableDict.ContainsKey(commands[1]) == true)
                        {
                            bool success2 = int.TryParse(commands[3], out num);
                            var = VariableDict[commands[1]];
                            //if commands[3] is number 
                            if (success2)
                            {
                                if (var >= num)
                                {
                                    Console.WriteLine("Valid");
                                    run = true;
                                }
                            }
                            //if commands[3] is a pre set variable
                            else if (VariableDict.ContainsKey(commands[3]) == true)
                            {
                                int number = VariableDict[commands[3]];

                                if (var >= number)
                                {
                                    Console.WriteLine("Valid");
                                    run = true;
                                }
                            }
                            //if commands[3] is not a number or a pre set variable 
                            else
                            {
                                Console.WriteLine("invalid");
                                run = false;
                            }
                        }
                        else
                        {
                            Console.WriteLine("invalid 2");
                            run = false;
                        }
                    }
                    else if (check.Equals("<="))
                    {
                        //if commands[1] is a number
                        if (success)
                        {

                            bool success2 = int.TryParse(commands[3], out num);
                            //if commands[3] is a number
                            if (success2)
                            {
                                if (var <= num)
                                {
                                    Console.WriteLine("Valid");
                                    run = true;
                                }
                            }
                            //if commands[3] is a pre set variable
                            else if (VariableDict.ContainsKey(commands[3]) == true)
                            {
                                int number = VariableDict[commands[3]];

                                if (var <= number)
                                {
                                    Console.WriteLine("Valid");
                                    run = true;
                                }

                            }
                            //if commands[3] is not a number or a pre set variable
                            else
                            {
                                Console.WriteLine("not valid");
                            }
                        }
                        //if commands[1] is a pre set variable 
                        else if (VariableDict.ContainsKey(commands[1]) == true)
                        {

                            bool success2 = int.TryParse(commands[3], out num);
                            var = VariableDict[commands[1]];
                            //if commands[3] is number 
                            if (success2)
                            {
                                if (var <= num)
                                {
                                    Console.WriteLine("Valid");
                                    run = true;
                                }
                            }
                            //if commands[3] is a pre set variable
                            else if (VariableDict.ContainsKey(commands[3]) == true)
                            {
                                int number = VariableDict[commands[3]];

                                if (var <= number)
                                {
                                    Console.WriteLine("Valid");
                                    run = true;
                                }
                            }
                            //if commands[3] is not a number or a pre set variable 
                            else
                            {
                                Console.WriteLine("invalid");
                                run = false;
                            }
                        }
                        else
                        {
                            Console.WriteLine("invalid 2");
                            run = false;
                        }
                    }
                    else if (check.Equals("!="))
                    {
                        //if commands[1] is a number
                        if (success)
                        {

                            bool success2 = int.TryParse(commands[3], out num);
                            //if commands[3] is a number
                            if (success2)
                            {
                                if (var != num)
                                {
                                    Console.WriteLine("Valid");
                                    run = true;
                                }
                            }
                            //if commands[3] is a pre set variable
                            else if (VariableDict.ContainsKey(commands[3]) == true)
                            {
                                int number = VariableDict[commands[3]];

                                if (var != number)
                                {
                                    Console.WriteLine("Valid");
                                    run = true;
                                }

                            }
                            //if commands[3] is not a number or a pre set variable
                            else
                            {
                                Console.WriteLine("not valid");
                            }
                        }
                        //if commands[1] is a pre set variable 
                        else if (VariableDict.ContainsKey(commands[1]) == true)
                        {

                            bool success2 = int.TryParse(commands[3], out num);
                            var = VariableDict[commands[1]];
                            //if commands[3] is number 
                            if (success2)
                            {
                                if (var != num)
                                {
                                    Console.WriteLine("Valid");
                                    run = true;
                                }
                            }
                            //if commands[3] is a pre set variable
                            else if (VariableDict.ContainsKey(commands[3]) == true)
                            {
                                int number = VariableDict[commands[3]];

                                if (var != number)
                                {
                                    Console.WriteLine("Valid");
                                    run = true;
                                }
                            }
                            //if commands[3] is not a number or a pre set variable 
                            else
                            {
                                Console.WriteLine("invalid");
                                run = false;
                            }
                        }
                        else
                        {
                            Console.WriteLine("invalid 2");
                            run = false;
                        }
                    }
                }

                else
                {
                    label2.Text = "Incorrect operorator type: " + commands[2];
                }

            }
            else if (commands[0].Equals("endif"))
            {
                run = true;
            }
            else if (commands[0].Equals("loop"))
            {
                loopline = lineread;
                loop = true;
                Console.WriteLine("loop");
                index = index + 1;
                                
                try
                {
                    for (int currentline = lineread; currentline < lines; currentline++)
                    {

                        string[] command = commands[currentline].Split(' ');

                        if (run)
                        {
                            ReadCommands(command);
                        }
                        else
                        {
                            currentline = currentline + 1;
                            run = true;
                            Console.WriteLine("not run");
                        }
                    }
                }
                // If the program text box has a tailing blank line, then the number of lines to parse is 1 less
                catch
                {
                    lines = lines - 1;
                    for (int currentline = lineread; currentline < lines; currentline++)
                    {

                        string[] command = commands[currentline].Split(' ');

                        if (run)
                        {
                            ReadCommands(command);
                        }
                        else
                        {
                            currentline = currentline + 1;
                            run = true;
                            Console.WriteLine("not run");
                        }
                    }
                }

                loop = false;
            }
            else if (commands[0].Equals("end"))
            {
                if (iterations != index)
                {
                    loop = false;
                    Console.WriteLine("loop off");
                }
                else
                {
                    index = index + 1;
                }
            }
            else if (variable)
            {
                int number;
                try
                {
                    string symbol = commands[3];
                    string VarName = commands[0];
                    string var = commands[4];
                    string va = commands[2];
                    var.Trim('\n');
                    var.Trim('\r');
                    var.Trim(' ');
                    //x = x + y
                    bool success = VariableDict.ContainsKey(commands[2]);
                    if (success)
                    {
                        try
                        {
                            bool success2 = VariableDict.ContainsKey(var);
                            //Console.WriteLine(var);
                            Console.WriteLine("success: " + success);
                            Console.WriteLine("success2: " + success2);
                            
                            //name = VariableDict.ContainsKey(var);
                            
                            Console.WriteLine("Var: " + var);
                            if (success2)
                            {
                                //Console.WriteLine("Va: " + commands[2] + " : " + va);
                                if (symbol.Equals("+"))
                                {
                                    int var1 = VariableDict[commands[2]];
                                    int var2 = VariableDict[commands[4]];
                                    Console.WriteLine(var1 + " + " + var2);
                                    var1 = var1 + var2;
                                    Console.WriteLine(var1);

                                    VariableDict[VarName] = var1;
                                    Console.WriteLine("Var: " + VarName + " " + var1);

                                }
                                else if (symbol.Equals("-"))
                                {
                                    int var1 = VariableDict[commands[2]];
                                    int var2 = VariableDict[commands[4]];
                                    Console.WriteLine(var1 + " - " + var2);
                                    var1 = var1 - var2;
                                    Console.WriteLine("answer: " + var1);

                                    VariableDict[VarName] = var1;
                                    Console.WriteLine("Var: " + VarName + " " + var1);
                                }
                                else
                                {
                                    label2.Text = "Invalid symbol";
                                }
                            }
                            else if (int.TryParse(var, out number))
                            {
                                if (symbol.Equals("+"))
                                {
                                    int var1 = VariableDict[commands[2]];
                                    int var2 = Int32.Parse(commands[4]);
                                    Console.WriteLine(var1 + " + " + var2);
                                    var1 = var1 + var2;
                                    Console.WriteLine(var1);
                                    //Dictionary[Key] = NewValue;
                                    VariableDict[VarName] = var1;
                                    Console.WriteLine("Var: " + VarName + " " + var1);
                                }
                                else if (symbol.Equals("-"))
                                {
                                    int var1 = VariableDict[commands[2]];
                                    int var2 = Int32.Parse(commands[4]);
                                    Console.WriteLine(var1 + " + " + var2);
                                    var1 = var1 - var2;
                                    Console.WriteLine(var1);

                                    VariableDict[VarName] = var1;
                                    Console.WriteLine("Var: " + VarName + " " + var1);
                                }
                                else
                                {
                                    label2.Text = "Invalid symbol";
                                }
                            }
                            else
                            {
                                label2.Text = "invalid data used: " + var;
                                Console.WriteLine("error here");
                            }
                        }
                        catch
                        {
                            Console.WriteLine("error");
                        }
                    }
                    else if (int.TryParse(commands[2], out number))
                    {
                        try
                        {
                            bool success2 = VariableDict.ContainsKey(commands[4]);

                            if (success2)
                            {
                                if (symbol.Equals("+"))
                                {
                                    int var1 = VariableDict[commands[2]];
                                    int var2 = VariableDict[commands[4]];
                                    Console.WriteLine(var1 + " + " + var2);
                                    var1 = var1 + var2;
                                    Console.WriteLine(var1);

                                    VariableDict[VarName] = var1;
                                    Console.WriteLine("Var: " + VarName + " " + var1);

                                }
                                else if (symbol.Equals("-"))
                                {
                                    int var1 = VariableDict[commands[2]];
                                    int var2 = VariableDict[commands[4]];
                                    Console.WriteLine(var1 + " - " + var2);
                                    var1 = var1 - var2;
                                    Console.WriteLine("answer: " + var1);

                                    VariableDict[VarName] = var1;
                                    Console.WriteLine("Var: " + VarName + " " + var1);
                                }
                                else
                                {
                                    label2.Text = "Invalid symbol";
                                }
                            }
                            else if (int.TryParse(commands[4], out number))
                            {
                                if (symbol.Equals("+"))
                                {

                                    int var1 = Int32.Parse(commands[4]);
                                    int var2 = VariableDict[commands[2]];

                                    Console.WriteLine(var1 + " + " + var2);
                                    var1 = var1 + var2;
                                    Console.WriteLine(var1);
                                    //Dictionary[Key] = NewValue;
                                    VariableDict[VarName] = var1;
                                    Console.WriteLine("Var: " + VarName + " " + var1);
                                }
                                else if (symbol.Equals("-"))
                                {
                                    int var1 = Int32.Parse(commands[4]);
                                    int var2 = VariableDict[commands[2]];
                                    Console.WriteLine(var1 + " + " + var2);
                                    var1 = var1 - var2;
                                    Console.WriteLine(var1);

                                    VariableDict[VarName] = var1;
                                    Console.WriteLine("Var: " + VarName + " " + var1);
                                }
                                else
                                {
                                    label2.Text = "Invalid symbol";
                                }
                            }
                            else
                            {
                                label2.Text = "invalid data used: " + commands[4];
                                Console.WriteLine("error6");
                            }

                        }
                        catch
                        {
                            Console.WriteLine("error");
                        }
                    }
                    else
                    {
                        Console.WriteLine("error2");
                        label2.Text = "invalid data used: " + commands[2];
                    }
                }
                catch
                {

                    Console.WriteLine("Var is no good");
                }
            }
            else
            {
                label1.Text = "Unknown command: " + commands[0];
            }


        }   

        /// <summary>
        /// saves the contents of the program text box to a text file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            //save program text
            using (SaveFileDialog saveFile = new SaveFileDialog())
            {
                saveFile.DefaultExt = ".txt";
                saveFile.FileName = "";

                if (saveFile.ShowDialog() == DialogResult.OK)
                {
                    File.WriteAllText(saveFile.FileName, Program.Text);
                }
            }
        }

        /// <summary>
        /// loads a text file into the contents of the program window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            //load program text
            using (OpenFileDialog openfile = new OpenFileDialog())
            {
                openfile.FileName = "";
                if (openfile.ShowDialog() == DialogResult.OK)
                {
                    Program.Text = File.ReadAllText(openfile.FileName);

                }
            }
            

        }
    }
}
